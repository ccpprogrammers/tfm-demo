<?php
ignore_user_abort(true);

// turn off gzip compression
if ( function_exists( 'apache_setenv' ) ) {
  apache_setenv( 'no-gzip', 1 );
}

ini_set('zlib.output_compression', 0);

// turn on output buffering if necessary
if (ob_get_level() == 0) {
  ob_start();
}

// removing any content encoding like gzip etc.
header('Content-encoding: none', true);

//check to ses if request is a POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // the GIF should not be POSTed to, so do nothing...
  echo ' ';
} else {
  $file = 'h3k-logo.png';
  header("Content-type: image/png");
  // needed to avoid cache time on browser side
  header("Content-Length: ".filesize($file));
  header("Cache-Control: private, no-cache, no-cache=Set-Cookie, proxy-revalidate");
  header("Expires: Wed, 11 Jan 2000 12:59:00 GMT");
  header("Last-Modified: Wed, 11 Jan 2006 12:59:00 GMT");
  header("Pragma: no-cache");
  readfile( 'h3k-logo.png' );   
}

// flush all output buffers. No reason to make the user wait for OWA.
ob_flush();
flush();
ob_end_flush();

//Get Public Ip address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

//Write Log file
function LogFile($msg) {
     $today = date("dMy"); 
     $logfile = $today."-log.txt"; 
     $dir = 'logs';
     $saveLocation=$dir . '/' . $logfile;
     if  (!$handle = @fopen($saveLocation, "a")) {
          exit;
     }
     else {
          if (@fwrite($handle,"$msg\r\n") === FALSE) {
               exit;
          }
   
          @fclose($handle);
     }
}


// DO ANALYTICS TRACKING HERE
if(isset($_GET['id']) && $_GET['id'] == 0) {
	$referer = $_GET['refer'];
	$ip = get_client_ip();
	$timezone  = 5; 
	$time2 = gmdate("j M Y H:i", time() + 3960*($timezone+date("I")));	
	$data = "[".$time2.'] {URI} '.$ip.' - '.$referer;
	// Write to the file
	LogFile($data);
}
?>
